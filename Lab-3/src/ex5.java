import becker.robots.*;
public class Ex5 {
	
	 public static void main(String[] args)
	   {
	      // Set up the initial situation
	      City ny = new City();
	  	  
	      Thing parcel = new Thing(ny, 2, 2);
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH);
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
	     
	      Wall blockAve4 = new Wall(ny, 1, 2, Direction.EAST);
	     
	      Wall blockAve6 = new Wall(ny, 1, 2, Direction.SOUTH);
	      Wall blockAve7 = new Wall(ny, 2, 1, Direction.SOUTH);
	      Robot mark = new Robot(ny, 1, 2, Direction.SOUTH);
	  	  
		  
	      // mark goes around the roadblock
	      mark.turnLeft();
	      mark.turnLeft();
	      mark.turnLeft();
	      mark.move();
	     
	      mark.turnLeft();
	      
	      mark.move();
	      mark.turnLeft();
	     
	      mark.move();
	      
	      mark.pickThing();
	      mark.turnLeft();
	      mark.turnLeft();
	      mark.move();
	      
	      mark.turnLeft();
	      mark.turnLeft();
	      mark.turnLeft();
	      
	      mark.move();
	      
	      mark.turnLeft();
	      mark.turnLeft();
	      mark.turnLeft();
	      
	      mark.move();
	      
	      mark.turnLeft();
	      mark.turnLeft();
	      mark.turnLeft();
	      mark.putThing();
	      
	      
	  }
	      

}
