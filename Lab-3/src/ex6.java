package packageg30126.neag.bogdan.lab3.ex6;


public class MyPoint {
	
	private int x, y; //Two instance variables x (int) and y (int).
	
	public MyPoint() //A �no-argument� (or �no-arg�) constructor that construct a point at (0, 0).
	{
		x=y=0;
	}
	
	public MyPoint(int x,int y)	//A constructor that constructs a point with the given x and y coordinates.
	{
		this.x=x;
		this.y=y;
	}
	
	
	//setter x
	void setX(int k){ //Getter and setter for the instance variables x and y.
    	
    		this.x=x;
    }
	
	//setter y
	void setY(int z){
		this.y=y;
		
	}
	
	//getter x
    int getx(){
    	return x;
    	
    }
    
    //getter y
    int gety(){
    	return y;
    	
    }