package g30126.rogojanu.george.lucian.L2.ex6;

import java.util.Scanner;

public class factorial {
	public static void main(String[]args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		if (N<0) 
			System.out.println("Numarul introdus nu este corect!");
		else 
			System.out.println(factorialrec(N) + "\n" + factorialnerec(N));
	}
	
	static int factorialrec(int a) {
		if(a==0) return 1;
		else
		return a*factorialrec(a-1);
	}
	
	static int factorialnerec(int a) {
		int rezultat = 1;
		while(a>0) {
			rezultat*=a;
			a--;
		}
		return rezultat;
	}

}
