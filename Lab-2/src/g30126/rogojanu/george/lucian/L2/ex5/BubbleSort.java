package g30126.rogojanu.george.lucian.L2.ex5;

import java.util.Arrays;
import java.util.Random;

public class BubbleSort {
	public static void main(String[]args) {
		Random a = new Random();
		int N = 50, aux;
		int[] vector = new int [N];
		for(int i=0;i<N;i++) {
			vector[i] = a.nextInt();
		}
		
		System.out.println(Arrays.toString(vector));
		
		for(int i=0;i<N;i++) {
			for(int j=0;j<N-1;j++) {
				if(vector[j]>vector[j+1]) {
					aux=vector[j+1];
					vector[j+1]=vector[j];
					vector[j]=aux;
					
				}
			}
		}
		System.out.println("Vectorul sortat este: \n" + Arrays.toString(vector));
	}

}
