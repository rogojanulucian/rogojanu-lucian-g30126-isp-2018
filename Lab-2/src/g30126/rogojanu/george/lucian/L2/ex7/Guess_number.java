package g30126.rogojanu.george.lucian.L2.ex7;

import java.util.Random;
import java.util.Scanner;

public class Guess_number {
	public static void main(String[]args) {
		Random a = new Random();
		int nr = a.nextInt();
		int i=0;
		int nr_citit;
		boolean b = true;
		while(i<3 && b) {
			System.out.println("Guess the number:");
			Scanner in = new Scanner(System.in);
			nr_citit = in.nextInt();
			if(nr>nr_citit) 
			{
				System.out.println("Wrong answer, your number is too low!");
			}
			else if(nr<nr_citit)
			{
				System.out.println("Wrong answer, your number is too high!");
			}
			else if(nr==nr_citit)
			{
				System.out.println("You guessed the number, you won!");
				b=false;
			}
			i++;
			if (i==3 && b) {
				System.out.println("You lost!");
			}
		}
	}

}
