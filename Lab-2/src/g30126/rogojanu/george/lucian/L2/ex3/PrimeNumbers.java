package g30126.rogojanu.george.lucian.L2.ex3;

import java.util.Scanner;

public class PrimeNumbers {
	
	static boolean nr_prim(int a) {
		int r,i;
		for(i=2;2*i<=a;i++) {
			r=a%i;
			if(r==0) return false;
		}
		return true;
	}
	
	public static void main(String[]args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Introduceti capetele intervalului intre care doriti sa cautati numerele prime");
		int a = in.nextInt();
		int b = in.nextInt();
		int i,nr=1;
		for(i=a;i<=b;i++) {
			nr_prim(i);
			if(nr_prim(i)==true) {
				System.out.println(nr + ". " + i);
				nr++;
			}
		}
		
		
	}

}
