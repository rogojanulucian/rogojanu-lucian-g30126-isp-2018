package g30126.RogojanuLucian.L4.e5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import g30126.pop.marian.l4.e4.Author;

class BookTest {

	@Test
	void testToString() {
		System.out.println("Execute test 1");
		Author a = new Author("J. K. Rolling","rollingstones@email.com",'f');
		Book b = new Book("Harry Potter", a, 23.32, 50);
		assertEquals(b.toString(),"'book-Harry Potter' by author-J. K. Rolling(f) at rollingstones@email.com");
	}

}
