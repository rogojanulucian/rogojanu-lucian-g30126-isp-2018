package g30126.RogojanuLucian.L4.e3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CircleTest {

	@Test
	void testGetRadius() {
		Circle c = new Circle(1);
		assertEquals(c.getRadius(),1);
	}

	@Test
	void testGetArea() {
		Circle c =new Circle(4);
		assertEquals(50.26,c.getArea(),0.01);
	}
}

