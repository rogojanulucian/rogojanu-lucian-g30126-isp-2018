package g30126.RogojanuLucian.L4.e3;

public class Circle {
	
	private double radius;
	private String color;
	
	public Circle(){
		radius = 1;
		color = "red";
	}
	
	public Circle(double radius){
		this.radius=radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return Math.PI*radius*radius;
	}
	
	public static void main(String[] args) {
		Circle c1 = new Circle();
		System.out.println(c1.getRadius() + "\n" + c1.getArea());
		Circle c2 = new Circle(4);
		System.out.println(c2.getRadius() + "\n" + c2.getArea());
	}

}
