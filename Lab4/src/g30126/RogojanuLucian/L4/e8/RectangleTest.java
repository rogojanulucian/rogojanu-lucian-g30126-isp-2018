package g30126.RogojanuLucian.L4.e8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RectangleTest {

	@Test
	void testToString() {
		System.out.println("Execute test 1");
		Rectangle r = new Rectangle(1,2);
		assertEquals("A Rectangle with width=1.0 and length=2.0, which is a subclass of A Shape with color of green and filled true",r.toString());
	}

	@Test
	void testGetArea() {
		System.out.println("Execute test 2");
		Rectangle r =new Rectangle(1,2);
		assertEquals(2,r.getArea());
	}

	@Test
	void testGetPerimeter() {
		System.out.println("Execute test 3");
		Rectangle r = new Rectangle(1,2);
		assertEquals(6,r.getPerimeter());
	}

}
