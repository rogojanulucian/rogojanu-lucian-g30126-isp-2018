package g30126.RogojanuLucian.L4.e8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CircleTest {

	@Test
	void testToString() {
		System.out.println("Execute test 1");
		Circle c = new Circle(1,"green",true);
		assertEquals("A Circle with radius=1.0, which is a subclass of A Shape with color of green and filled true",c.toString());
	}

	@Test
	void testGetArea() {
		System.out.println("Execute test 2");
		Circle c = new Circle(1);
		assertEquals(3.14,c.getArea(),0.01);
	}

	@Test
	void testGetPerimeter() {
		System.out.println("Execute test 3");
		Circle c = new Circle(1);
		assertEquals(2*3.14,c.getPerimeter(),0.01);
	}

}
