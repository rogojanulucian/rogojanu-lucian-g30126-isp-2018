package g30126.RogojanuLucian.L4.e8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SquareTest {

	@Test
	void testToString() {
		System.out.println("Execute test 1");
		Square p = new Square(1);
		assertEquals("A Square with side=1.0, which is a subclass of A Rectangle with width=1.0 and length=1.0, which is a subclass of A Shape with color of green and filled true",p.toString());
	}

}
