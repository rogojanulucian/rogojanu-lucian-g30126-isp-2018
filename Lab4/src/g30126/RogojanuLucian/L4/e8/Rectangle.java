package g30126.RogojanuLucian.L4.e8;

class Rectangle extends Shape{
	private double width;
	private double length;
	
	public Rectangle() {
		width = 1;
		length = 1;
	}
	
	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
	}
	
	public Rectangle(double width, double length, String color, boolean filled) {
		super(color,filled);
		this.width = width;
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	public double getArea() {
		return getLength()*getWidth();
	}
	
	public double getPerimeter() {
		return 2*getLength() + 2*getWidth();
	}
	
	public String toString() {
		return "A Rectangle with width=" + getWidth() +" and length=" + getLength() +  ", which is a subclass of " + super.toString();
	}
	
}