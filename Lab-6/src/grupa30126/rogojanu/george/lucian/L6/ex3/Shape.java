package grupa30126.rogojanu.george.lucian.L6.ex3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
	
	void draw(Graphics g);

}

