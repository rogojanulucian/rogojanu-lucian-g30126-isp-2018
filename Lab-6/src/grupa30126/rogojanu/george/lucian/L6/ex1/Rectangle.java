package grupa30126.rogojanu.george.lucian.L6.ex1;

import java.awt.*;

public class Rectangle extends Shape{
 
    private int length;
 
    public Rectangle(Color color, int length, int x, int y, String id, boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
    }
 
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill() == false) {
        	g.drawRect(getX(), getY(), length, length+30);
        }
        else 
        	g.fillRect(getX(), getY(), length, length+30);
    }
}