package g30126.rogojanu.george.lucian.L5.ex3;

public abstract class Sensor {
	private String location;
	
	public abstract int readValue();
	
	public String getLocation() {
		return location;
	}

}
