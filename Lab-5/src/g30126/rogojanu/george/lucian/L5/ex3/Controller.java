package g30126.rogojanu.george.lucian.L5.ex3;

public class Controller {
	 public TemperatureSensor tempSensor;
	 public LightSensor lightSensor;
	
	 public void control() {
	  for(int i = 0; i < 20 ; i++)
	  { 
	   tempSensor = new TemperatureSensor();
	   lightSensor = new LightSensor();
	   System.out.println(tempSensor.readValue());
	   System.out.println(lightSensor.readValue());
	   try {
		Thread.sleep(1000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	  }
	  
	 }
	 	 
	 public static void main (String[] args) {
	  Controller c = new Controller();
	  c.control();
	 }

	 
	 /**Singleton variant*/
	 private static Controller control;
	 
	 private Controller() {
	 }
	 
	 public static Controller getControl() {
		 if (control == null) {
			 control = new Controller();
		 }
		 return control;
	 }
	 
}