package g30126.rogojanu.george.lucian.L5.ex3;

import java.util.Random;

public class LightSensor extends Sensor {
	 int value;
	 public LightSensor() {
	 Random rand = new Random();
	 value = rand.nextInt(100);
	 }
	 public int readValue() {
	  return this.value;
	 }
	}
	