package g30126.rogojanu.george.lucian.L5.ex2;

public class RealImage implements Image {
	
	private String fileName;
	
	public RealImage(String fileName) {
		this.fileName = fileName;
		loadFromDisk(fileName);
	}
	
	public void display() {
		System.out.println("Displaying " + fileName);
	}
	
	private void loadFromDisk(String fileName) {
		System.out.println("Loading " + fileName);
	}
	
	public static void main(String[] args) {
		String file = "flori";
		ProxyImage img = new ProxyImage(file, true);
		img.display();
	}

}

