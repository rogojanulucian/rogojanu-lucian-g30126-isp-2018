package g30126.rogojanu.george.lucian.L5.ex2;

public class RotatedImage implements Image{
	String fileName;
	public RotatedImage(String fileName) {
		this.fileName = fileName;
		loadFromDisk(fileName);
	}
	
	private void loadFromDisk(String fileName) {
		System.out.println("Loading " + fileName);
	}
	
	public void display() {
		System.out.println("Display rotated " + fileName);
	}
}
