package g30126.rogojanu.george.lucian.L5.ex2;

public class ProxyImage implements Image {
	
	//private RealImage realImage;
	private String fileName;
	private boolean b;
	private Image img;
	
	public ProxyImage(String fileName, boolean b) {
		this.fileName = fileName;
		this.b = b;
	}
	
	public void display() {
		if(b==false) {
			img = new RealImage(fileName);
		}
		else {
			img = new RotatedImage(fileName);
		}
		img.display();
		/*if(realImage == null) {
			realImage = new RealImage(fileName);
		}
		realImage.display();
		*/
	}

}
