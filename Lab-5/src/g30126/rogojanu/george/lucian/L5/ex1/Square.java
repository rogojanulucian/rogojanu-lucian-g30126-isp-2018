package g30126.rogojanu.george.lucian.L5.ex1;

public class Square extends Rectangle {
	
	public Square() {
		this.length = 1;
		this.width =1;
	}
	
	public Square(double side) {
		this.length = side;
		this.width = side;
	}
	
	public Square(double side, String color, boolean filled) {
		this.length = side;
		this.width = side;
		this.color = color;
		this.filled = filled;
	}
	
	public double getSide() {
		return getLength();
	}
	
	public void setSide(double side) {
		setWidth(side);
		setLength(side);
	}
	
	public void setWidth(double side) {
		setWidth(side);
	}
	
	public void setLength(double side) {
		setLength(side);
	}
	
	public String toString() {
		return "A Square with side =" + getSide() +
				" color =" + color +
				" filled =" + filled;
	}

}
