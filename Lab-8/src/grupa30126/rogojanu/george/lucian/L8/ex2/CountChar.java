package grupa30126.rogojanu.george.lucian.L8.ex2;

import java.io.*;
import java.io.IOException;

public class CountChar {
	

	public CountChar() {
		
		
	}
	
	public void countCharacters(BufferedReader intrare) throws IOException{
		String linie = new String();
		int i = 0;
		System.out.println("Introduceti un caracter, iar eu va voi spune exact "
				+ "de cate ori apare in fisierul data.txt");
			char c;
			c = (char) System.in.read();
			while((linie = intrare.readLine()) != null) {
				for(int j = 0; j<linie.length(); j++) {
					if(linie.charAt(j) == c) {
						i++;
					}
				}
			}
			System.out.println("Character " + c + " appears " + i + " times.");
			
				
	}
	
}
