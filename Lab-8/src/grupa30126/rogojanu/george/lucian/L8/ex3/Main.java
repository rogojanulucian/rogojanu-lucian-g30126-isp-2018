package grupa30126.rogojanu.george.lucian.L8.ex3;

import java.io.IOException;
import java.io.*;


public class Main {
	public static void main(String[] args) {
		
		char comanda;
		BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			Enigma machine = new Enigma();
		do {
		System.out.println("Meniu:");
		System.out.println("e - Encrypt a file");
		System.out.println("d - Decrypt a file");
		System.out.println("x - Exiting the program Enigma");
		
		comanda = fluxIn.readLine().charAt(0);
		
		switch(comanda) {
		case 'e': case 'E':
			machine.encrypt(); break;
		case 'd': case 'D': 
			machine.decrypt(); break;
		}
			
		}while((comanda != 'x') && (comanda != 'X'));
		System.out.println("Good bye! See you next time!");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}

}
