package grupa30126.rogojanu.george.lucian.L7.ex4;

public class Word {
	
	private String name;
	
	Word(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
