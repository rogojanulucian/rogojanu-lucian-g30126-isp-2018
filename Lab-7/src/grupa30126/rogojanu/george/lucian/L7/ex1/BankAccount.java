package grupa30126.rogojanu.george.lucian.L7.ex1;

import java.util.Comparator;

public class BankAccount  implements Comparable<BankAccount>{
	
	private String owner;
	private double balance;
	
	
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public BankAccount(String owner, double balance){
		this.owner = owner;
		this.balance  = balance;
	}
	
	public void withdraw(double amount) {
		setBalance(balance-amount);
		System.out.println("A fost retrasa suma de " + amount + "din contul lui " + getOwner() + " Noua suma: " +getBalance());
	}
	
	public void deposit(double amount) {
		setBalance(balance+amount);
		System.out.println("A fost depusa suma de " + amount + "in contul lui "+ getOwner() + " Noua suma: " +getBalance());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(BankAccount x) {
		BankAccount t = (BankAccount) x;
		if(balance-t.balance < 0)
		return -1;
		else if(balance-t.balance == 0)
			return 0;
		else return 1;
	}
	
	public static Comparator<BankAccount> OwnerNameComp = new Comparator<BankAccount>() {
		public int compare(BankAccount b1, BankAccount b2) {
			String b1Owner = b1.getOwner().toUpperCase();
			String b2Owner = b2.getOwner().toUpperCase();
			return b1Owner.compareTo(b2Owner);
		}
	};

}
