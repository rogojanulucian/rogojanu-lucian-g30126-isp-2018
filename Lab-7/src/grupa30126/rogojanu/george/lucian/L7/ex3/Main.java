package grupa30126.rogojanu.george.lucian.L7.ex3;

public class Main {
	public static void main(String[] args) {
		BankWithTree bank = new BankWithTree();
		bank.addAccount("Sam", 100);
		bank.addAccount("Johny", 23.23);
		bank.addAccount("Hermione", 1234.99);
		bank.addAccount("Laurence", 2.3);
		bank.printAccounts();
		bank.printAccounts(20, 200);
		bank.addAccount("Misu", 495.56);
		bank.addAccount("Sivester", 15.0);
		bank.printAccounts(10,500);
		bank.getAccount("Johny");
		bank.getAllAcounts();
		
	}

}
