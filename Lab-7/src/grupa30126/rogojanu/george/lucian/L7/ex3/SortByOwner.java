package grupa30126.rogojanu.george.lucian.L7.ex3;

import java.util.Comparator;

import g30126.pop.marian.l7.e1.BankAccount;

public class SortByOwner implements Comparator<BankAccount> {
	@Override
	public int compare(BankAccount b1, BankAccount b2) {
		return b1.getOwner().toUpperCase().compareTo(b2.getOwner().toUpperCase());
	}
}